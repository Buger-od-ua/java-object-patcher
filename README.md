# Java Object Patcher
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.gitlab.buger-od-ua/object-patcher/badge.svg)](https://maven-badges.herokuapp.com/maven-central/object-patcher)
[![pipeline status](https://gitlab.com/Buger-od-ua/java-object-patcher/badges/master/pipeline.svg)](https://gitlab.com/Buger-od-ua/java-object-patcher/-/commits/master)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)

Java Object Patcher is simple, Java 8 compatible, dependency-free and fast library which makes patching/merging objects simple.

## Problem
I had big pojo with dozens of fields and PATCH request which should update my pojo following some rules.
Which suppose to look something like this:
```java
if (condition) {
    savedObject.setField1(patchObject.getField1());
}
//and repeat this for each field of my pojo
```
I hated to reproduce this ugly boilerplate piece of code so came up with solution based on Java Reflections API.
There is simple core idea behind it which I want to extend and add more functionality to this library.

## Install
It could be installed with Maven:
```xml
<dependency>
    <groupId>com.gitlab.buger-od-ua</groupId>
    <artifactId>object-patcher</artifactId>
    <version>1.0.0</version>
</dependency>
```
Or gradle:
```groovy
compile "com.gitlab.buger-od-ua:object-patcher:1.0.0"
```

## In action
There are multiple `Strategy`s with self-explanatory names:
- ALWAYS_TARGET
- TARGET_IF_SOURCE_NULL
- ALWAYS_SOURCE
- SOURCE_IF_TARGET_NULL
- AlWAYS_NULL

They could be applied on any class fields by annotation `@PatchStrategy` as follows:
```java
@PatchStrategy(Strategy.ALWAYS_SOURCE)
private final String someField;
```
Now you can patch object with `StrategyBasedObjectPatcher`:
```java
StrategyBasedObjectPatcher patcher = new StrategyBasedObjectPatcher();
patcher.patch(sourcePojo, targetPojo);
```



