package com.buger.patcher;

import com.buger.patcher.exception.PatchException;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 27-09-2020
 */
public interface ObjectPatcher {

    <S, T extends S> T patch(S source, T target) throws PatchException;

}
