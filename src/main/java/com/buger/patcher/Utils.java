package com.buger.patcher;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 29-09-2020
 */
public class Utils {

    public static Set<Field> getAllFields(Class<?> type) {
        Set<Field> fields = new HashSet<Field>();
        for (Class<?> c = type; c != null; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }

    public static Set<Field> getCommonFields(Class<?> class1, Class<?> class2) {
        Set<Field> class1Fields = getAllFields(class1);
        Set<Field> class2Fields = getAllFields(class2);

        Set<Field> result = new HashSet<Field>();
        for (Field class1Field : class1Fields) {
            if (class2Fields.contains(class1Field)) {
                result.add(class1Field);
            }
        }

        return result;
    }

}
