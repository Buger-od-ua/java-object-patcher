package com.buger.patcher.exception;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 29-09-2020
 */
public class FieldValueResolverException extends Exception {

    public FieldValueResolverException() {
        super();
    }

    public FieldValueResolverException(String message) {
        super(message);
    }

    public FieldValueResolverException(String message, Throwable cause) {
        super(message, cause);
    }

    public FieldValueResolverException(Throwable cause) {
        super(cause);
    }

}
