package com.buger.patcher.exception;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public class PatchException extends Exception {

    public PatchException() {
        super();
    }

    public PatchException(String message) {
        super(message);
    }

    public PatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public PatchException(Throwable cause) {
        super(cause);
    }

}
