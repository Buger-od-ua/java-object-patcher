package com.buger.patcher.resolver;

import com.buger.patcher.exception.FieldValueResolverException;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public interface FieldValueResolver {

    Object resolveFieldValue(Object source, Object target) throws FieldValueResolverException;

}
