package com.buger.patcher.resolver.factory;

import com.buger.patcher.resolver.FieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public interface FieldValueResolverFactory {

    FieldValueResolver createFieldPatcher(Field field);

}
