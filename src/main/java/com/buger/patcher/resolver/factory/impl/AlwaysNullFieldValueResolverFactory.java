package com.buger.patcher.resolver.factory.impl;

import com.buger.patcher.resolver.factory.FieldValueResolverFactory;
import com.buger.patcher.resolver.impl.AlwaysNullFieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 29-09-2020
 */
public class AlwaysNullFieldValueResolverFactory implements FieldValueResolverFactory {

    public AlwaysNullFieldValueResolver createFieldPatcher(Field field) {
        return new AlwaysNullFieldValueResolver();
    }

}
