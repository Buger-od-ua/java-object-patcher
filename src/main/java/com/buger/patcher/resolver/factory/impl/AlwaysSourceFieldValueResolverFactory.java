package com.buger.patcher.resolver.factory.impl;

import com.buger.patcher.resolver.factory.FieldValueResolverFactory;
import com.buger.patcher.resolver.impl.AlwaysSourceFieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public class AlwaysSourceFieldValueResolverFactory implements FieldValueResolverFactory {

    public AlwaysSourceFieldValueResolver createFieldPatcher(Field field) {
        return new AlwaysSourceFieldValueResolver(field);
    }

}
