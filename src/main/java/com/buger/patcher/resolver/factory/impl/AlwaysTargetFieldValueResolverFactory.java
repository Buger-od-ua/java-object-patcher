package com.buger.patcher.resolver.factory.impl;

import com.buger.patcher.resolver.factory.FieldValueResolverFactory;
import com.buger.patcher.resolver.impl.AlwaysTargetFieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public class AlwaysTargetFieldValueResolverFactory implements FieldValueResolverFactory {

    public AlwaysTargetFieldValueResolver createFieldPatcher(Field field) {
        return new AlwaysTargetFieldValueResolver(field);
    }

}
