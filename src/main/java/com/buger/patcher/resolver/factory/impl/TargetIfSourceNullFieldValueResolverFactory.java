package com.buger.patcher.resolver.factory.impl;

import com.buger.patcher.resolver.factory.FieldValueResolverFactory;
import com.buger.patcher.resolver.impl.TargetIfSourceNullFieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 29-09-2020
 */
public class TargetIfSourceNullFieldValueResolverFactory implements FieldValueResolverFactory {

    public TargetIfSourceNullFieldValueResolver createFieldPatcher(Field field) {
        return new TargetIfSourceNullFieldValueResolver(field);
    }

}
