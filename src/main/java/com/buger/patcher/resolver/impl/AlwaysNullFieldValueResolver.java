package com.buger.patcher.resolver.impl;

import com.buger.patcher.exception.FieldValueResolverException;
import com.buger.patcher.resolver.FieldValueResolver;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 29-09-2020
 */
public class AlwaysNullFieldValueResolver implements FieldValueResolver {

    public Object resolveFieldValue(Object source, Object target) throws FieldValueResolverException {
        return null;
    }

}
