package com.buger.patcher.resolver.impl;

import com.buger.patcher.exception.FieldValueResolverException;
import com.buger.patcher.resolver.FieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public class AlwaysSourceFieldValueResolver implements FieldValueResolver {

    private final Field field;

    public AlwaysSourceFieldValueResolver(Field field) {
        this.field = field;
    }

    public Object resolveFieldValue(Object source, Object target) throws FieldValueResolverException {
        try {
            return field.get(source);
        } catch (IllegalAccessException e) {
            throw new FieldValueResolverException();
        }
    }

}
