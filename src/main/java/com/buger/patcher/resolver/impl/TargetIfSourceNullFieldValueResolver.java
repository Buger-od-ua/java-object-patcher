package com.buger.patcher.resolver.impl;

import com.buger.patcher.exception.FieldValueResolverException;
import com.buger.patcher.resolver.FieldValueResolver;

import java.lang.reflect.Field;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 29-09-2020
 */
public class TargetIfSourceNullFieldValueResolver implements FieldValueResolver {

    private final Field field;

    public TargetIfSourceNullFieldValueResolver(Field field) {
        this.field = field;
    }

    public Object resolveFieldValue(Object source, Object target) throws FieldValueResolverException {
        try {
            Object sourceFieldValue = field.get(source);
            Object result;
            if (sourceFieldValue == null) {
                result = field.get(target);
            } else {
                result = sourceFieldValue;
            }
            return result;
        } catch (IllegalAccessException e) {
            throw new FieldValueResolverException();
        }
    }

}
