package com.buger.patcher.strategy;

import com.buger.patcher.resolver.FieldValueResolver;
import com.buger.patcher.resolver.factory.FieldValueResolverFactory;
import com.buger.patcher.resolver.factory.impl.*;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 28-09-2020
 */
public class FieldStrategyBasedFieldValueResolverFactory {

    private final Map<Strategy, FieldValueResolverFactory> factoryMap = new HashMap<Strategy, FieldValueResolverFactory>();

    public FieldStrategyBasedFieldValueResolverFactory() {
        factoryMap.put(Strategy.ALWAYS_TARGET, new AlwaysTargetFieldValueResolverFactory());
        factoryMap.put(Strategy.ALWAYS_SOURCE, new AlwaysSourceFieldValueResolverFactory());
        factoryMap.put(Strategy.TARGET_IF_SOURCE_NULL, new TargetIfSourceNullFieldValueResolverFactory());
        factoryMap.put(Strategy.SOURCE_IF_TARGET_NULL, new SourceIfTargetNullFieldValueResolverFactory());
        factoryMap.put(Strategy.AlWAYS_NULL, new AlwaysNullFieldValueResolverFactory());
    }

    public FieldValueResolver createFieldPatcher(Field field) {
        PatchStrategy patchStrategy = field.getAnnotation(PatchStrategy.class);
        FieldValueResolverFactory factory;
        FieldValueResolver result;
        if (patchStrategy != null) {
            Strategy strategy = patchStrategy.value();
            factory = factoryMap.get(strategy);
            if (factory != null) {
                result =factory.createFieldPatcher(field);
            }
            else {
                result = null;
            }
        } else {
            result = null;
        }
        return result;
    }

}
