package com.buger.patcher.strategy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 27-09-2020
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PatchStrategy {

    Strategy value() default Strategy.ALWAYS_SOURCE;

}
