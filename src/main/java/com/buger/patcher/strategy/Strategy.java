package com.buger.patcher.strategy;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 27-09-2020
 */
public enum Strategy {
    ALWAYS_TARGET,
    TARGET_IF_SOURCE_NULL,
    ALWAYS_SOURCE,
    SOURCE_IF_TARGET_NULL,
    AlWAYS_NULL
}
