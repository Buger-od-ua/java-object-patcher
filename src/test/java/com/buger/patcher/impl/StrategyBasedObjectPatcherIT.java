package com.buger.patcher.impl;

import com.buger.patcher.strategy.PatchStrategy;
import com.buger.patcher.strategy.Strategy;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Created by Buheria Oleksii {@literal buheriaoleksii@gmail.com}
 * @version 1.0
 * @since 27-09-2020
 */
class StrategyBasedObjectPatcherIT {

    private final StrategyBasedObjectPatcher merger = new StrategyBasedObjectPatcher();

    public static abstract class TestPojoParent {

        @PatchStrategy(Strategy.ALWAYS_SOURCE)
        protected final String parentStr;

        @PatchStrategy(Strategy.ALWAYS_TARGET)
        protected final int parentNum;

        public TestPojoParent(String parentStr, int parentNum) {
            this.parentStr = parentStr;
            this.parentNum = parentNum;
        }

        public String getParentStr() {
            return parentStr;
        }

        public int getParentNum() {
            return parentNum;
        }
    }
    public static abstract class TestPojoChild extends TestPojoParent{

        @PatchStrategy(Strategy.SOURCE_IF_TARGET_NULL)
        private final String childStr;

        @PatchStrategy(Strategy.TARGET_IF_SOURCE_NULL)
        private final Integer childNum;

        public TestPojoChild(String parentStr, int parentNum, String childStr, Integer childNum) {
            super(parentStr, parentNum);
            this.childStr = childStr;
            this.childNum = childNum;
        }

        public String getChildStr() {
            return childStr;
        }

        public Integer getChildNum() {
            return childNum;
        }

    }

    public static class TestPojoChild2 extends TestPojoChild{

        @PatchStrategy(Strategy.SOURCE_IF_TARGET_NULL)
        private final String child2Str;

        @PatchStrategy(Strategy.TARGET_IF_SOURCE_NULL)
        private final Integer child2Num;

        public TestPojoChild2(String parentStr, int parentNum, String childStr, Integer childNum, String child2Str, Integer child2Num) {
            super(parentStr, parentNum, childStr, childNum);
            this.child2Str = child2Str;
            this.child2Num = child2Num;
        }

        public String getChild2Str() {
            return child2Str;
        }

        public Integer getChild2Num() {
            return child2Num;
        }

    }

    @Test
    @DisplayName("test1")
    public void test1() throws Exception {
        TestPojoChild2 s = new TestPojoChild2("s_p", 1, "s_c", null, "s_c2", 5);
        TestPojoChild2 t = new TestPojoChild2("t_p", 2, null, 4, "t_c2", 6);
        merger.patch(s, t);
        assertEquals(t.getParentStr(), "s_p");
        assertEquals(t.getParentNum(), 2);
        assertEquals(t.getChildStr(), "s_c");
        assertEquals(t.getChildNum(), 4);
        assertEquals(t.getChild2Str(), "t_c2");
        assertEquals(t.getChild2Num(), 5);
    }

}